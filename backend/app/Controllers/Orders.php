<?php

namespace App\Controllers;
use App\Models\OrdersModel;
use CodeIgniter\RESTful\ResourceController;

class Orders extends ResourceController
{
    function __construct(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header("Access-Control-Allow-Headers: X-Requested-With");
        header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding");
        header("Access-Control-Allow-Methods: PUT, POST, GET, OPTIONS, DELETE");
        $this->orders = new OrdersModel();
    }
    
    
    public function getAllOrders()
    {
        $data = $this->orders->findAll();
        return $this->respond($data);
    }
    
    public function getOrdersById()
    {
        $id = $this->request->uri->getSegment(2);
        $data = $this->orders->find($id);
        return $this->respond($data);
    }
    
    public function createOrders()
    {
        $input = $this->request->getVar();
        $res = $this->orders->save($input);
        return $this->respond($res);
    }
    
    public function UpdateOrders()
    {
        $id = $this->request->uri->getSegment(2);
        $input = $this->request->getVar();
        $res = $this->orders->update($id, $input);
        return $this->respond($res); 
        
    }
    
    public function deleteOrders()
    {
        $id = $this->request->uri->getSegment(2);
        $res = $this->orders->delete($id);
        return $this->respond($res);
    }
}