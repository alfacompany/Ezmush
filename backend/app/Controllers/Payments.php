<?php

namespace App\Controllers;
use App\Models\PaymentsModel;
use CodeIgniter\RESTful\ResourceController;

class Payments extends ResourceController
{
    function __construct(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header("Access-Control-Allow-Headers: X-Requested-With");
        header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding");
        header("Access-Control-Allow-Methods: PUT, POST, GET, OPTIONS, DELETE");
        $this->payments = new PaymentsModel();
    }
    
    
    public function getAllPayments()
    {
        $data = $this->payments->findAll();
        return $this->respond($data);
    }
    
    public function getPaymentsById()
    {
        $id = $this->request->uri->getSegment(2);
        $data = $this->payments->find($id);
        return $this->respond($data);
    }
    
    public function createPayments()
    {
        $input = $this->request->getVar();
        $res = $this->payments->save($input);
        return $this->respond($res);
    }
    
    public function UpdatePayments()
    {
        $id = $this->request->uri->getSegment(2);
        $input = $this->request->getVar();
        $res = $this->payments->update($id, $input);
        return $this->respond($res); 
        
    }
    
    public function deletePayments()
    {
        $id = $this->request->uri->getSegment(2);
        $res = $this->payments->delete($id);
        return $this->respond($res);
    }
}