<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'id_user';
    protected $useAutoIncrement = true;

    protected $allowedFields = ['username','password','name','address','email','telp','role', 'created_at'];
}