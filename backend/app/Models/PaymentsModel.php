<?php

namespace App\Models;

use CodeIgniter\Model;

class PaymentsModel extends Model
{
    protected $table      = 'payments';
    protected $primaryKey = 'id_payment';
    protected $useAutoIncrement = true;

    protected $allowedFields = ['id_order','bukti','status','nama_rekening','jumlah_bayar'];
}